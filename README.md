# rbac

This script creates a namespace, role, rolebinding, serviceaccount and a corresponding kubeconfig für a new namespace. The token used in the kubeconfig is a long term token and is meant to use in ci/cd pielines.

## usage

```
git clone https://gitlab.com/workshop21/open-source/rbac
cd rbac
./createRBAC.sh <namespace>
```

To keep the generated kubeconfig file add flag `--nocleanup`.

