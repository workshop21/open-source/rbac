#!/bin/bash


###
# FUNCTIONS
###

check_if_envsubst_installed() {
  command -v envsubst
  if [[ $? != 0 ]]; then
    echo "envsubst is not installed. exiting"
    exit 1
  fi
}

generate_yaml() {
  export K8S_NAMESPACE=$1
  for f in templates/*.yaml
  do
    if [[ ! -d generated ]]; then
  	mkdir -p generated
    fi
    envsubst < $f > "generated/$(basename $f)"
    if [[ $? != 0 ]];
    then
      exit 1
    fi
  done
}

print_kubeconfig()
{
  echo ""
  cat kubeconfig/kubeconfig_${K8S_NAMESPACE}.yaml | base64
  echo ""
  cat kubeconfig/kubeconfig_${K8S_NAMESPACE}.yaml
  echo ""
}

createkubeconfig() {
  export K8S_NAMESPACE=$1
  TOKEN_NAME=$(kubectl get secret -n $K8S_NAMESPACE | grep ${K8S_NAMESPACE}-deploy | cut -d " " -f 1)
  export TOKEN=$(kubectl describe secret -n ${K8S_NAMESPACE} $TOKEN_NAME | grep -i '^token' | cut -d ":" -f 2 | tr -d ' ')
  envsubst < kubeconfig/kubeconfig_template.yaml > "kubeconfig/kubeconfig_${K8S_NAMESPACE}.yaml"
  if [[ $? != 0 ]];
  then
    exit 1
  fi
}

create_namespace()
{
  export K8S_NAMESPACE=$1
  kubectl get ns ${K8S_NAMESPACE} 2> /dev/null|| kubectl create ns ${K8S_NAMESPACE}
}



cleanup() {
  export K8S_NAMESPACE=$1
  rm -f kubeconfig/kubeconfig_${K8S_NAMESPACE}.yaml
  if [[ $? != 0 ]];
  then
    exit 1
  fi
}
###
# MAIN
###
if [[ $# -eq 0 || ${1} = "help" ]]
  then
    echo "No arguments supplied"
    echo "usage: ./createRBAC <namespace>"
    echo "if no cleanup is desired add --cleanup e.g. ./createRBAC dummy --nocleanup"
    exit 0
fi

create_namespace ${1}
generate_yaml ${1}
kubectl apply -f generated
createkubeconfig ${1}
print_kubeconfig ${1} 
if [[ ${2} = "--nocleanup" ]];
  then
    echo "no cleanup"
    exit 0
fi
cleanup ${1}

